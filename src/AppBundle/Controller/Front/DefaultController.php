<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Form\CommentType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/",name="app.front.default.index")
     */

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

//        $posts = $em->getRepository('AppBundle:Post')->findAll();

//        $posts = $em->getRepository(Post::class)
//            ->createQueryBuilder('post')
//            ->where("post.status = :status")
//            ->orderBy("post.createdAt", "DESC")
//            ->setParameter('status',20)
//            ->getQuery()
//            ->getResult();

        $posts = $em->getRepository(Post::class)->getPostList();

        $paginator = $this->container->get('knp_paginator');

        $pagination = $paginator->paginate(
            $posts,
            $request->get('page',1),
            10
        );

        // Bez doctrine

//        $conn = $this->get('database_connection');
//        $posts = $conn->query('select * from post where status = 20 limit 5')
//            ->fetchAll();
        return $this->render('front/default/index.html.twig',array(
            'posts' => $pagination
        ));
    }

    /**
     * @Route("/post/{slug}", name="app.front.default.show")
     */
    public function showAction(Request $request, Post $post) // Wykorzystanie ParamConverter
    {
        $em = $this->getDoctrine()->getManager();
//
//        $post = $em->getRepository(Post::class)->findOneBySlug($slug);
//
//        if (!$post){
//            throw new NotFoundHttpException();
//        }

        $comments = new Comment();
        $form = $this->createForm(CommentType::class, $comments);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $comments->setIp($request->getClientIp());
            $comments->setPost($post);

            $em->persist($comments);
            $em->flush();

            $this->container->get('blog.mailer')
                ->sendConfirmation($comments->getEmail())
            ;
            return $this->redirectToRoute('app.front.default.show',array(
                'slug' => $post->getSlug()
            ));
        }

        return $this->render('front/default/show.html.twig',array(
            'post' => $post,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/firewall",name="app.default.front.firewall")
     */
    public function firewallTestAction(Request $request)
    {

        $user = $this->getUser();

        dump($user);

        return $this->render('front/default/firewall.html.twig',array(
            'user' => $user
        ));
    }
}
