<?php
/**
 * Created by PhpStorm.
 * User: kurs
 * Date: 27.10.17
 * Time: 12:37
 */

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class Mailer
{
    private $mailer;

    private $container;

    public function __construct(\Swift_Mailer $mailer, ContainerInterface $container)
    {
        $this->mailer = $mailer;
        $this->container = $container;
    }


    public function sendConfirmation($email)
    {
        $message = \Swift_Message::newInstance();
        $message
            ->setTo($email)
            ->setSubject('Potwierdzenie')
            ->setBody('Dziękujemy za komentarz')
            ->setFrom('test@test.pl')
        ;

        $this->mailer->send($message);
    }

}